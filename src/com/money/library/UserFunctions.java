package com.money.library;

import java.util.HashMap;
import org.json.JSONObject;

public class UserFunctions {
	
	private JSONParser jsonParser = new JSONParser();
	
	private static String URL = "http://www.domenicoromano.it/ordernow/userFunctions.php";
	private HashMap<String, String> values = new HashMap<String, String>();
	private String user_name = "Francesco";
	private String user_mail = "francesco@mail.com";
	private String tag_register = "register";
	
	public JSONObject RegisterUser(){
        // Building Parameters
		
		values.put("tag", tag_register);
		values.put("user_name", user_name);
        values.put("user_mail", user_mail);
        
        JSONObject json=jsonParser.getJSONFromUrl(URL, values);
        //JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
        //return json;

        return json;
        //Log.e("JSON", json.toString());
        //return json;
    }

}
