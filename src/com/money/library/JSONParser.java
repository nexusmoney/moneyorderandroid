package com.money.library;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
 
public class JSONParser {
	
	HttpURLConnection conn;
    static JSONObject jObj = null;
    static JSONArray jArr;
    static String json = "";
    private String sb = new String();
    static int code;
 
    // constructor
    public JSONParser() {
 
    }
    
    public JSONObject getJSONFromUrl(String url, HashMap<String, String> postDataParams) {
    	
    	try {
    		 // defaultHttpClient
        	URL urlConn = new URL(url);
        	conn = (HttpURLConnection) urlConn.openConnection();
        	
        	//add request header
        	conn.setRequestMethod("POST");
        	conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
        	
        	// Send post request
        	conn.setDoOutput(true);
        	conn.setDoInput(true);
        	DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        	wr.writeBytes(getPostDataString(postDataParams));
            wr.flush();
            wr.close();

            code = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
    		System.out.println("Post parameters : " + getPostDataString(postDataParams));
    		System.out.println("Response Code : " + code);
    		
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            
            while ((line = in.readLine()) != null) {
            	sb+=line;
            }
            System.out.println("ciao"+sb);
            //jArr = new JSONArray(sb);
            jObj = (JSONObject) new JSONTokener(sb).nextValue();

            
        } catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return jObj;
    }


public JSONArray getJSONArrayFromUrl(String url, HashMap<String, String> postDataParams) {
    // Making HTTP request
	try {
		 // defaultHttpClient
    	URL urlConn = new URL(url);
    	conn = (HttpURLConnection) urlConn.openConnection();
    	
    	//add request header
    	conn.setRequestMethod("POST");
    	conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
    	
    	// Send post request
    	conn.setDoOutput(true);
    	conn.setDoInput(true);
    	DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
    	wr.writeBytes(getPostDataString(postDataParams));
        wr.flush();
        wr.close();

        code = conn.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + getPostDataString(postDataParams));
		System.out.println("Response Code : " + code);

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        String line;
       
        while ((line = in.readLine()) != null) {
        	sb+=line;
        }
        System.out.println("ciao"+sb);
        //jArr = new JSONArray(sb);
        jObj = (JSONObject) new JSONTokener(sb).nextValue();

        System.out.println("stica"+jObj.get("arraytest"));
        
    } catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return jArr;
}

	private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;
	    for(Map.Entry<String, String> entry : params.entrySet()){
	        if (first)
	            first = false;
	        else
	            result.append("&");
	
	        result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
	        result.append("=");
	        result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
	    }
	
	    return result.toString();
}
}