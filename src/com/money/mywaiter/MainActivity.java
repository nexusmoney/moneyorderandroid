package com.money.mywaiter;

import org.json.JSONException;
import org.json.JSONObject;

import com.money.library.UserFunctions;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {
	
	private Button buttonTest;
	private UserFunctions userfunction;
	private TextView textStatus;
	private JSONObject json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);
    	 
        
        buttonTest = (Button)findViewById(R.id.button1);
        textStatus = (TextView)findViewById(R.id.textView1);
        
        userfunction = new UserFunctions();
     // Login button Click Event
        buttonTest.setOnClickListener(new View.OnClickListener() {
        	
        	public void onClick(View view){
        		runThread();
        	}
        	});
        }

    private void runThread(){
    	runOnUiThread(new Thread(new Runnable() {
			public void run() {
				// your logic
				json = userfunction.RegisterUser();
				try {
					System.out.println(json.get("success"));
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			}));
    	try {
			textStatus.setText(json.get("success").toString());
			} catch (JSONException e) {
				e.printStackTrace();
				}
    	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
